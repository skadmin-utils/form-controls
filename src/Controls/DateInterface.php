<?php

declare(strict_types=1);

namespace SkadminUtils\FormControls\Controls;

use Exception;
use Nette\Forms\Container;
use DateTimeInterface;
use DateTime;
use Nette\Forms\Controls\TextInput;

class DateInterface extends TextInput
{

    public const TypeSingle = 'single';

    private string $format;
    private bool $clear;
    private string $type;

    public function __construct(?string $label = null, string $format = 'd.m.Y', bool $clear = true, string $type = self::TypeSingle)
    {
        $this->format = $format;
        $this->clear = $clear;
        $this->type = $type;

        parent::__construct($label);
    }

    public static function register(string $controlName = 'addDateInterface'): void
    {
        Container::extensionMethod(
            'Nette\Forms\Container::' . $controlName,
            static function ($form, string $name, ?string $label = null, string $format = 'd.m.Y', bool $clear = true, string $type = self::TypeSingle): self {
                $control = new self($label, $format, $clear, $type);

                switch ($type) {
                    case self::TypeSingle:
                        $control = $control->setHtmlAttribute('data-date');
                        break;
                }

                if ($clear) {
                    $control = $control->setHtmlAttribute('data-date-clear');
                }

                return $form[$name] = $control;
            }
        );
    }

    public function setValue(mixed $value): self
    {
        if ($value instanceof DateTimeInterface) {
            $value = $value->format($this->format);
        }

        return parent::setValue($value);
    }

    public function setDefaultValue(mixed $value): self
    {
        if ($value instanceof DateTimeInterface) {
            $value = $value->format($this->format);
        }

        return parent::setDefaultValue($value);
    }

    public function getValue(): mixed
    {
        $value = parent::getValue();

        if (trim($value) === '') {
            return null;
        }

        return \DateTime::createFromFormat($this->format, $value);
    }

}
