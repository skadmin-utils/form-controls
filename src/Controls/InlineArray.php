<?php

declare(strict_types=1);

namespace SkadminUtils\FormControls\Controls;

use Exception;
use Nette\Forms\Container;
use Nette\Forms\Controls\TextInput;
use Nette\Utils\Html;
use Nette\Utils\Strings;

use function count;
use function explode;
use function implode;
use function is_array;
use function sprintf;

class InlineArray extends TextInput
{
    private const SEPARATOR_VALUE = '::::';
    private const SEPARATOR_ITEM  = ':!!:';

    public static function register(string $controlName = 'addInlineArray'): void
    {
        Container::extensionMethod(
            'Nette\Forms\Container::' . $controlName,
            static function ($form, $name, $label = null, array $array = []): InlineArray {
                $control                = new self($label);
                $control->control->type = 'hidden';

                $control->setHtmlAttribute('data-inline-array', 1);

                if (count($array) > 0) {
                    $control->setDefaultValue($array);
                }

                return $form[$name] = $control;
            }
        );
    }

    public function setValue(mixed $value): InlineArray
    {
        // @phpstan-ignore-next-line
        if (! is_array($value) && ! empty($value) && ! Strings::contains($value, self::SEPARATOR_VALUE)) {
            throw new Exception('Value for InlineArray must by array!');
        }

        // @phpstan-ignore-next-line
        return parent::setValue($this->processArrayToString($value));
    }

    public function setDefaultValue(mixed $value): InlineArray
    {
        // @phpstan-ignore-next-line
        if (! is_array($value) && ! empty($value)) {
            throw new Exception('Value for InlineArray must by array!');
        }

        // @phpstan-ignore-next-line
        return parent::setDefaultValue($this->processArrayToString($value));
    }

    /**
     * @param string|string[]|null $value
     */
    public static function processArrayToString(string|array|null $value): ?string
    {
        // @phpstan-ignore-next-line
        if (empty($value)) {
            return null;
        }

        if (! is_array($value) && Strings::contains($value, self::SEPARATOR_VALUE)) {
            return $value;
        }

        $defaultvalue = [];

        if (is_array($value)) {
            foreach ($value as $key => $_value) {
                $defaultvalue[] = sprintf('%s%s%s', $key, self::SEPARATOR_VALUE, $_value);
            }
        }

        return implode(self::SEPARATOR_ITEM, $defaultvalue);
    }

    public function getValue(): mixed
    {
        return $this->processStringToArray(parent::getValue());
    }

    /**
     * @return array<mixed>
     */
    private function processStringToArray(?string $value): array
    {
        // @phpstan-ignore-next-line
        if (empty($value)) {
            return [];
        }

        $items = explode(self::SEPARATOR_ITEM, $value);

        $resultArray = [];
        foreach ($items as $item) {
            [$key, $_value] = explode(self::SEPARATOR_VALUE, $item);
            $resultArray[$key] = $_value;
        }

        return $resultArray;
    }

    /**
     * @param $caption
     * @return Html|string|null
     */
    public function getLabel($caption = null)
    {
        if ($this->caption === null) {
            return null;
        }

        return parent::getLabel($caption);
    }
}
