<?php

declare(strict_types=1);

namespace SkadminUtils\FormControls\Controls;

use Exception;
use Nette\Forms\Container;
use Nette\Forms\Controls\SelectBox;
use Nette\Forms\Controls\TextInput;
use Nette\Utils\Html;
use Nette\Utils\Strings;

use SkadminUtils\Utils\Utils\ObjectGather;
use function count;
use function explode;
use function implode;
use function is_array;
use function sprintf;

class SelectEntity extends SelectBox
{

    private $entityOptions = [];
    private $identKey;
    private $identValue;
    private $identItem;

    public function __construct(?string $label = null, array $items = [], callable|string $identKey = 'id', callable|string $identValue = 'name', ?callable $identItem = null)
    {
        $this->identKey = $identKey;
        $this->identValue = $identValue;
        $this->identItem = $identItem;

        parent::__construct($label, $items);
    }

    public function setItems(array $items, bool $useKeys = true)
    {
        $this->entityOptions = [];

        return parent::setItems($this->transformOptions($items), $useKeys);
    }


    public static function register(string $controlName = 'addSelectEntity'): void
    {
        Container::extensionMethod(
            'Nette\Forms\Container::' . $controlName,
            static function ($form, $name, $label = null, array $items = [], callable|string $identKey = 'id', callable|string $identValue = 'name', ?callable $identItem = null): self {
                $control = new self($label, $items, $identKey, $identValue, $identItem);
                return $form[$name] = $control;
            }
        );
    }

    private function transformOptions(array $items): array
    {
        $identKey = $this->identKey;
        $identValue = $this->identValue;
        $identItem = $this->identItem;

        $options = [];
        foreach ($items as $item) {
            if (is_callable($identKey)) {
                $pureIdentKey = $identKey($item);
            } elseif (is_object($item)) {
                $pureIdentKey = ObjectGather::get($item, $identKey);
            }

            if (is_callable($identValue)) {
                $pureIdentValue = $identValue($item);
            } else {
                $pureIdentValue = ObjectGather::get($item, $identValue);
            }

            if (is_callable($identItem)) {
                $item = $identItem($item);
            }
            $this->entityOptions[$pureIdentKey] = $item;
            $options[$pureIdentKey] = $pureIdentValue;
        }

        return $options;
    }

    public function setValue(mixed $value): self
    {
        if (is_callable($this->identKey)) {
            $identKey = $this->identKey;
            $value = $identKey($value);
        } else {
            $value = ObjectGather::get($value, $this->identKey);
        }

        return parent::setValue($value);
    }

    public function setDefaultValue(mixed $value): self
    {
        if (is_callable($this->identKey)) {
            $identKey = $this->identKey;
            $value = $identKey($value);
        } else {
            $value = ObjectGather::get($value, $this->identKey);
        }

        return parent::setDefaultValue($value);
    }

    public function getValue(): mixed
    {
        $value = parent::getValue();
        if ($value === null || (is_string($value) && trim($value) === '')) {
            return null;
        }

        return $this->entityOptions[parent::getValue()];
    }

}
