<?php

declare(strict_types=1);

namespace SkadminUtils\FormControls\DI;

use Nette\DI\CompilerExtension;
use Nette\PhpGenerator\ClassType;

class ControlsExtension extends CompilerExtension
{
    public function afterCompile(ClassType $class): void
    {
        parent::afterCompile($class);

        $init = $class->getMethod('initialize');
        $init->addBody('SkadminUtils\FormControls\Controls\InlineArray::register();');
        $init->addBody('SkadminUtils\FormControls\Controls\SelectEntity::register();');
        $init->addBody('SkadminUtils\FormControls\Controls\DateInterface::register();');
    }
}
