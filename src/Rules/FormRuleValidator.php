<?php

declare(strict_types=1);

//namespace App\Model\System;

namespace SkadminUtils\FormControls\Rules;

use Nette\Forms\Controls\MultiSelectBox;
use Nette\Forms\Controls\SelectBox;
use Nette\Forms\IControl;

use function in_array;

final class FormRuleValidator
{
    // pravidla jsou specifikovány v js static/js/nette-form-validators.js
    public const IS_SELECTED = 'SkadminUtils\FormControls\Rules\FormRuleValidator::isSelected';

    public static function isSelected(IControl $control, mixed $value): bool
    {
        if ($control instanceof MultiSelectBox) {
            $values = $control->getValue();
        } elseif ($control instanceof SelectBox) {
            $values = [$control->getValue()];
        } else {
            $values = [];
        }

        return in_array($value, $values, true);
    }
}
