<?php

declare(strict_types=1);

namespace SkadminUtils\FormControls\UI;

use Contributte\FormMultiplier\Multiplier;
use Contributte\ReCaptcha\Forms\InvisibleReCaptchaField;
use Contributte\ReCaptcha\Forms\ReCaptchaField;
use Nette\Forms\Container;
use Nette\Forms\Controls\TextInput;
use Skadmin\Translator\SimpleTranslation;
use SkadminUtils\FormControls\Controls\DateInterface;
use SkadminUtils\FormControls\Controls\InlineArray;

use SkadminUtils\FormControls\Controls\SelectEntity;
use function implode;
use function is_array;
use function sprintf;
use function str_replace;

class Form extends \Nette\Application\UI\Form
{

    public function addReCaptchaInput(): ReCaptchaField
    {
        return $this->addReCaptcha('recaptcha');
    }

    public function addReCaptcha(string $name, string $label = 'Captcha', bool $required = true, string $message = 'form.recaptcha.message'): ReCaptchaField
    {
        return parent::addReCaptcha($name, $label, $required, $message)
            ->setRequired('form.re-captcha.required');
    }

    public function addInvisibleReCaptchaInput(): ?InvisibleReCaptchaField
    {
        return $this->addInvisibleReCaptcha('recaptcha')
            ->setRequired('form.re-captcha.required');
    }

    public function addInvisibleReCaptcha(string $name, bool $required = true, string $message = 'form.recaptcha.message'): InvisibleReCaptchaField
    {
        return parent::addInvisibleReCaptcha($name, $required, $message);
    }

    /**
     * @param array<mixed> $array
     */
    public function addInlineArray(string $name, ?string $label = null, array $array = []): InlineArray
    {
        return parent::addInlineArray($name, $label, $array); // @phpstan-ignore-line
    }

    /**
     * @param array<mixed> $items
     */
    public function addSelectEntity(string $name, ?string $label = null, array $items = [], callable|string $identKey = 'id', callable|string $identValue = 'name', ?callable $identItem = null): SelectEntity
    {
        return parent::addSelectEntity($name, $label, $items, $identKey, $identValue, $identItem); // @phpstan-ignore-line
    }

    public function addDateInterface(string $name, ?string $label = null, string $format = 'd.m.Y', bool $clear = true, string $type = DateInterface::TypeSingle): DateInterface
    {
        return parent::addDateInterface($name, $label, $format, $clear, $type);
    }

    public function addPasswordStrong(string $name, ?string $label = null, ?int $cols = null, ?int $maxLength = null): TextInput
    {
        /*
         * .*[A-Z].*[A-Z].*                     Ensure string has two uppercase letters.
         * .*[@#$&^*+-=/\.?!%,":;[]{}<>].*      Ensure string has one special case letter.
         * .*[0-9].*[0-9].*                     Ensure string has two digits.
         * .*[a-z].*[a-z].*[a-z].*              Ensure string has three lowercase letters.
         * min lenght 8                         Ensure string is of length 8.
         */

        $inputPassword = parent::addPassword($name, $label, $cols, $maxLength);
        $inputPassword
            ->addRule(self::PATTERN, 'form.password-strong.rule.two-uppercase-letter', '.*[A-Z].*[A-Z].*')
            ->addRule(self::PATTERN, 'form.password-strong.rule.one-special-letter - @#$&^*+-=/\.?!%,":;[]{}<>', '.*[@#$&^*+-=/\\\.\?!%,":;\[\]{}<>].*')
            ->addRule(self::PATTERN, 'form.password-strong.rule.two-digit', '.*[0-9].*[0-9].*')
            ->addRule(self::PATTERN, 'form.password-strong.rule.three-lowercase-letter', '.*[a-z].*[a-z].*[a-z].*')
            ->addRule(self::MIN_LENGTH, 'form.password-strong.rule.min-length - 8', 8);

        return $inputPassword;
    }

    public function addPasswordMedium(string $name, ?string $label = null, ?int $cols = null, ?int $maxLength = null): TextInput
    {
        /*
         * .*[A-Z].*                            Ensure string has one uppercase letters.
         * .*[@#$&^*+-=/\.?!%,":;[]{}<>].*      Ensure string has one special case letter.
         * .*[0-9].*                            Ensure string has one digits.
         * min lenght 8                         Ensure string is of length 8.
         */

        $inputPassword = parent::addPassword($name, $label, $cols, $maxLength);
        $inputPassword
            ->addRule(self::PATTERN, 'form.password-medium.rule.one-uppercase-letter', '.*[A-Z].*')
            ->addRule(self::PATTERN, 'form.password-medium.rule.one-special-letter - @#$&^*+-=/\.?!%,":;[]{}<>', '.*[!@#$&^*+-=/\\\.\?!%,":;\[\]{}<>].*')
            ->addRule(self::PATTERN, 'form.password-medium.rule.one-digit', '.*[0-9].*')
            ->addRule(self::MIN_LENGTH, 'form.password-medium.rule.min-length - 8', 8);

        return $inputPassword;
    }

    public function addPhone(string $name, ?string $label = null, string $phonePattern = '^[+]?[()/0-9. -]{9,}$'): TextInput
    {
        $inputPhone = parent::addText($name, $label);
        $inputPhone
            ->addCondition(self::FILLED, true)
            ->addRule(self::PATTERN, 'form.phone.rule', $phonePattern);

        return $inputPhone;
    }

    /**
     * @param string|string[] $domain
     */
    public function addUrl(string $name, ?string $label = null, string|array $domain = 'https://www.skadmin.cz'): TextInput
    {
        if (is_array($domain)) {
            $patterns = [];

            foreach ($domain as $dom) {
                $patterns[] = $this->preparePattern($dom);
            }

            $pattern = sprintf('.*(%s).*', implode('|', $patterns));
            $errorMessage = new SimpleTranslation('form.url.rule.not-found-domain - [%s]', implode(', ', $domain));
        } else {
            $pattern = sprintf('.*%s.*', $this->preparePattern($domain));
            $errorMessage = new SimpleTranslation('form.url.rule.not-found-domain - %s', $domain);
        }

        $inputUrl = parent::addText($name, $label);
        $inputUrl
            ->addCondition(self::FILLED)
            ->addRule(self::PATTERN, $errorMessage, $pattern)
            ->addRule(self::PATTERN, 'form.url.rule.not-found-protocol', '^(http|https):\/\/.*');

        return $inputUrl;
    }

    public function addImageWithRFM(string $name, ?string $label = null): Container
    {
        $container = parent::addContainer($name);
        $identifier = $container->addUpload('identifier', $label)
            ->addRule(self::IMAGE, sprintf('%s.rule-image', $label));
        $identifierText = $container->addText('identifier_text');

        $identifierText
            ->addCondition(self::BLANK)
            ->toggle($identifier->getHtmlId())
            ->toggle(sprintf('%s-icon', $identifier->getHtmlId()))
            ->endCondition()
            ->addCondition(self::FILLED)
            ->toggle($identifierText->getHtmlId());

        return $container;
    }

    public function addMultiplier(string $name, callable $factory, int $copyNumber = 1, ?int $maxCopies = null): Multiplier
    {
        return parent::addMultiplier($name, $factory, $copyNumber, $maxCopies); // @phpstan-ignore-lines
    }

    private function preparePattern(string $pattern): string
    {
        return str_replace('.', '\.', $pattern);
    }
}
