<?php

declare(strict_types=1);

namespace SkadminUtils\FormControls\UI;

use App\AdminModule\Presenters\CoreAdminPresenter;
use App\Components\Control;
use App\CoreModule\Presenters\CorePresenter;
use Nette\SmartObject;
use Skadmin\Translator\Translator;

use function defined;
use function file_exists;
use function str_replace;

class FormControl extends Control
{
    use SmartObject;

    private ?string $template = null;

    protected Translator $translator;

    /** @var callable[] */
    public array $onFlashmessage = [];

    /** @var callable[] */
    public array $onRedraw = [];

    /** @var callable[] */
    public array $onSuccess = [];

    /** @var callable[] */
    public array $onSubmit = [];

    /** @var callable[] */
    public array $onRedirect = [];

    /** @var callable[] */
    public array $onBack = [];

    /** @var callable[] */
    public array $onError = [];

    /** @var callable[] */
    public array $onSendEmail = [];

    /** @var callable[] */
    public array $onModifyForm = [];

    protected CorePresenter $presenter;

    public function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }

    public function setControlTemplate(string $template): void
    {
        $this->template = $template;
    }

    public function getControlTemplate(string $defaultTemplate): string
    {
        $template = $this->template ?? $defaultTemplate;

        if (defined(CoreAdminPresenter::class . '::VersionBS')) {
            $resultTemplate = str_replace('.latte', '-bs' . CoreAdminPresenter::VersionBS . '.latte', $defaultTemplate);
        } else {
            $resultTemplate = $template;
        }

        if (! file_exists($resultTemplate)) {
            $resultTemplate = $template;
        }

        return $resultTemplate;
    }
}
