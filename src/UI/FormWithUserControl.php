<?php

declare(strict_types=1);

namespace SkadminUtils\FormControls\UI;

use Nette\Security\User;
use Nette\SmartObject;
use Skadmin\Translator\Translator;

class FormWithUserControl extends FormControl
{
    use SmartObject;

    protected User $loggedUser;

    public function __construct(Translator $translator, User $user)
    {
        parent::__construct($translator);
        $this->loggedUser = $user;
    }

    protected function isAllowed(string $resource, string $privilege): bool
    {
        return $this->loggedUser->isAllowed($resource, $privilege);
    }
}
