<?php

declare(strict_types=1);

namespace SkadminUtils\FormControls\Utils;

use Nette\Application\UI\ITemplateFactory;
use Nette\Forms\Container;
use Nette\Forms\Controls\Checkbox;
use Nette\Forms\Controls\UploadControl;
use Nette\Http\FileUpload;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;
use Skadmin\Translator\Translator;
use SkadminUtils\ImageStorage\ImageStorage;
use SkadminUtils\Utils\Utils\SystemDir;

use function assert;
use function basename;
use function file_get_contents;
use function implode;
use function is_file;
use function trim;
use function urldecode;

use const DIRECTORY_SEPARATOR;

class UtilsFormControl
{
    private ITemplateFactory $templateFactory;

    private Translator $translator;

    private static ImageStorage $imageStorage;

    private static SystemDir $systemDir;

    public function __construct(ITemplateFactory $ITemplateFactory, Translator $translator, ImageStorage $imageStorage, SystemDir $systemDir)
    {
        $this->templateFactory = $ITemplateFactory;
        $this->translator      = $translator;
        self::$imageStorage    = $imageStorage;
        self::$systemDir       = $systemDir;
    }

    public function renderFormFile(UploadControl $formFile): ?Html
    {
        $template = $this->templateFactory->createTemplate();
        $template->setFile(implode(DIRECTORY_SEPARATOR, [__DIR__, 'templates', 'renderFormFile.latte']));
        $template->setTranslator($this->translator);

        $template->formFile = $formFile;

        $htmlTemplate = new Html();
        $htmlTemplate->setHtml((string) $template); // @phpstan-ignore-line

        return $htmlTemplate;
    }

    public function renderFormCheckbox(Checkbox $formCheckbox, ?string $class = null): ?Html
    {
        $template = $this->templateFactory->createTemplate();
        $template->setFile(implode(DIRECTORY_SEPARATOR, [__DIR__, 'templates', 'renderFormCheckbox.latte']));
        $template->setTranslator($this->translator);

        $template->formCheckbox = $formCheckbox;
        $template->class        = $class;

        $htmlTemplate = new Html();
        $htmlTemplate->setHtml((string) $template); // @phpstan-ignore-line

        return $htmlTemplate;
    }

    public function renderFormCheckboxV5(Checkbox $formCheckbox, ?string $class = null): ?Html
    {
        $template = $this->templateFactory->createTemplate();
        $template->setFile(implode(DIRECTORY_SEPARATOR, [__DIR__, 'templates', 'renderFormCheckboxV5.latte']));
        $template->setTranslator($this->translator);

        $template->formCheckbox = $formCheckbox;
        $template->class        = $class;

        $htmlTemplate = new Html();
        $htmlTemplate->setHtml((string) $template); // @phpstan-ignore-line

        return $htmlTemplate;
    }

    public function renderFormImage(Container $formImageWithRfm, ?string $imagePreview): ?Html
    {
        $template = $this->templateFactory->createTemplate();
        $template->setFile(implode(DIRECTORY_SEPARATOR, [__DIR__, 'templates', 'renderFormImage.latte']));
        $template->setTranslator($this->translator);

        $template->imageStorage     = self::$imageStorage;
        $template->formImageWithRfm = $formImageWithRfm;
        $template->imagePreview     = $imagePreview;

        $htmlTemplate = new Html();
        $htmlTemplate->setHtml((string) $template); // @phpstan-ignore-line

        return $htmlTemplate;
    }

    public function renderFormImageWithRFM(Container $formImageWithRfm, ?string $imagePreview): ?Html
    {
        $template = $this->templateFactory->createTemplate();
        $template->setFile(implode(DIRECTORY_SEPARATOR, [__DIR__, 'templates', 'renderFormImageWithRFM.latte']));
        $template->setTranslator($this->translator);

        $template->imageStorage     = self::$imageStorage;
        $template->formImageWithRfm = $formImageWithRfm;
        $template->imagePreview     = $imagePreview;

        $htmlTemplate = new Html();
        $htmlTemplate->setHtml((string) $template); // @phpstan-ignore-line

        return $htmlTemplate;
    }

    public static function getImagePreview(ArrayHash $formImageWithRfm, string $namespace): ?string
    {
        if (! isset($formImageWithRfm->identifier) || ! $formImageWithRfm->identifier instanceof FileUpload) {
            return null;
        }

        $identifier = null;

        $imagePreview_identifier = $formImageWithRfm->identifier;
        assert($imagePreview_identifier instanceof FileUpload);

        if (isset($formImageWithRfm->identifier_text)) {
            $imagePreview_text = trim($formImageWithRfm->identifier_text);
        } else {
            $imagePreview_text = '';
        }

        if ($imagePreview_text !== '') {
            $imagePreview_text = urldecode(self::$systemDir->getPathRfm($imagePreview_text));

            if (is_file($imagePreview_text)) {
                $identifier = self::$imageStorage->saveContent(file_get_contents($imagePreview_text), basename($imagePreview_text), $namespace)->identifier;
            }
        } elseif ($imagePreview_identifier->isOk() && $imagePreview_identifier->isImage()) {
            $identifier = self::$imageStorage->saveUpload($imagePreview_identifier, $namespace)->identifier;
        }

        return $identifier;
    }
}
