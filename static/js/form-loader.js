var FormLoading = {
    icon: 'fal fa-spinner-third fa-spin',
    callback: function ($input) {
        $input.hide()
            .after($('<span class="' + $input.attr('class') + ' disabled">' + $input.val() + '<i class="' + FormLoading.icon + ' ms-2"></i></span>'));

        setTimeout(function () {
            $input.prop("disabled", true);
        }, 200);
    }
};

$.nette.ext({
    success: function () {
        formLoadingInit();
    }, init: function () {
        formLoadingInit();
    }
});

function formLoadingInit() {
    $('form.js-form-loading:not(.init-js-form-loading) input[type="submit"], form.js-form-loading:not(.init-js-form-loading) button')
        .click(function () {
            var $that = $(this);
            var $form = $that.closest('form');

            if (Nette.validateForm($form[0]) && !$form.hasClass('init-js-form-loading')) {
                $form.addClass('init-js-form-loading');
                FormLoading.callback($that);
            }
        });
}